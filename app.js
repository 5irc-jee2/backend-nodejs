global._instances = new Map();

var app = require('express')(),
	server = require('http').createServer(app);

const SocketController = require('./app/controller/SocketController');
const MessageService = require('./app/service/MessageService'); 
//const EnveloppeDTO = require('./app/model/EnveloppeDTO');
const MessageModel = require('./app/model/MessageModel');


var message = new MessageModel(null, 'roombla', '1', 'salut a tous les amis', new Date());

//var map = new Map();
//map.set(1, message);

//var enveloppe = new EnveloppeDTO(0,'writeMessage', Array.from(map));

//MessageService.sendMessage(enveloppe);
MessageService.sendMessage(message);

const request = require('request');
request.get('http://localhost:8083/message/roombla', (error, response, body) => {
	if(error) {
        return console.dir(error);
    }
    console.dir(JSON.parse(body));

});

// Chargement de la page index.html
app.get('/usersLogged', function (req, res) {
  res.send(SocketController.getUsernames());
});

/*app.get('/test', function (req, res) {
  res.send();
});*/

server.listen(8084);
