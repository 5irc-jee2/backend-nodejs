class MessageModel {

    constructor(id, roomName, userId, message, date) {
        this.id = id;
        this.roomName = roomName;
        this.userId = userId;
        this.message = message;
        this.date = date;
    }
}

module.exports = MessageModel;