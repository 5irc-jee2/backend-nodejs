class EnveloppeDTO {

    constructor(uuid, methodName, parameters) {
        this.uuid = uuid;
        this.methodName = methodName;
        this.parameters = parameters;
    }
}

module.exports = EnveloppeDTO;