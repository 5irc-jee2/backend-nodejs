const stompit = require('stompit');

const connectOptions = {
    'host': 'localhost',
    'port': 61613
};

const headers = {
    'destination': '/queue/queue_message'
};

class MessageService {
    constructor({}) {
        global._instances.set(MessageService.name, this);
    }

    static getInstance() {
        if (!global._instances.get(MessageService.name)) {
            return new MessageService({});
        }

        return global._instances.get(MessageService.name);
    }

    sendMessage(message) {
        stompit.connect(connectOptions, (error, client) => {
	    if (error) {
	        return console.error(error);
	    }

	    const frame = client.send(headers);
	    frame.write(JSON.stringify(message));
	    frame.end();

	    client.disconnect();
	});
    }
}

module.exports = MessageService.getInstance();