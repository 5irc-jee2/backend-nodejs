var app = require('express')(),
	server = require('http').createServer(app),
    io = require('socket.io').listen(server);

const MessageService = require('../service/MessageService'); 
const EnveloppeDTO = require('../model/EnveloppeDTO');
const MessageModel = require('../model/MessageModel');

class SocketController {

	
    
    constructor({}) {
		global._instances.set(SocketController.name, this);

		this.usernames = {};

        io.sockets.on('connection', function (socket) {

		    socket.on('login', function(username){
		        // store the username in the socket session for this client
		        socket.username = username;
		        // add the client's username to the global list
		        this.usernames[username] = username;
		    });

		    socket.on('joinroom', function(username){
		        // store the room name in the socket session for this client
		        // TODO : sécuriser cette merde
		        if(socket.username > username) {
		            socket.room = socket.username.concat(username);
		        } else {
		            socket.room = username.concat(socket.username);
		        }
		        // send client to room
		        socket.join(socket.room);

		         // update the view from userlist to chat
		        socket.emit('gotoroom', 'username');
		        // echo to client they've connected
		        socket.emit('updatechat', 'SERVER', 'You are connected to the chat');
		        // echo to room 1 that a person has connected to their room
		        socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username + ' has connected to the chat');
		    });

		    // when the client emits 'sendchat', this listens and executes
		    socket.on('sendchat', function (data) {
		        // we tell the client to execute 'updatechat' with 2 parameters
		        // TODO : Proteger contre injections ? 
		        io.sockets.in(socket.room).emit('updatechat', socket.username, data);
		    });

		    // when the user disconnects.. perform this
		    socket.on('leaveroom', function(){
		        // echo that this client has left
		        socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username + ' has disconnected');
		        // remove the username from global usernames list
		        socket.leave(socket.room);
		        socket.room = '';
		        // update the view from userlist to chat
		        socket.emit('gotolist');
		    });
		});
    }

    static getInstance() {
        if (!global._instances.get(SocketController.name)) {
            return new SocketController({});
        }

        return global._instances.get(SocketController.name);
    }

    getUsernames() {
        return this.usernames;
    }
}

module.exports = SocketController.getInstance();